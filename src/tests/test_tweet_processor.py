"""
test
"""
import sys
sys.path.append("/home/wassp/fun/sentiment-analysis-mlops/src/")
import unittest
from data.process_tweets import TweetProcessor



class TestTweetProcessor(unittest.TestCase):
    def test_remove_old_style_retweet(self):
        tweet_proc = TweetProcessor()
        no_RT = tweet_proc.remove_old_style_retweet(tweet)
        self.assertIsInstance(tweet, str)

